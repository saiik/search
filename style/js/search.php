<?php
	session_start();
	if(empty($_SESSION["hashed"])) {
		$logged = false;
	} else {
		$logged = true;
	}
?>
	var systems = [
		{system: "cbre"},
		{system: 'sipp'},
		{system: 'broadgate'},
		{system: 'jll'},
		{system: 'comserve'},
		{system: 'workspace'},
		{system: 'insight'},
		{system: 'leebaron'},
		{system: 'stockland'},
		{system: 'nfumutual'},
		{system: 'mprice'},
		{system: 'kinneygreen'},
		{system: 'berkleyestates'},
		{system: 'huawei'},
		{system: 'churchillservices'},
		{system: 'hilsonmoran'},
		{system: 'interserve'},
		{system: 'ibrc'},
		{system: 'praxis'},
		{system: 'prupim'},
		{system: 'colliers'},
		{system: 'orbit'},
		{system: 'lsh'},
		{system: 'aon'},
		{system: 'efm'}
	];

	var tables = [{
				 	user: {tables: [{table: 'uid'},{table: 'name'},{table: 'email'}]},
				   	customer: {tables: [{table: 'customer'},{table: 'addr_1'},{table: 'town'},{table: 'postcode'}]},
				   	service: {tables: [{table: 'name'},{table: 'email'}]}
				  }];

	String.prototype.capitalize = function() {
	    return this.charAt(0).toUpperCase() + this.slice(1);
	}


	//var systems = [{system: 'elog'}];

	var app = angular.module("searchApp", ['ngRoute']);

	app.config(function($routeProvider, $locationProvider) {
	    $routeProvider.when('/login/',
	        {
	            templateUrl:    'tpl/login.html',
	            controller: 	'loginController'
	        <?php if($logged === true) { ?>
	        }).when('/search/:siteId',
	        {
	            templateUrl:    'tpl/home.php',
	            controller: 	'jsonController'
	        }).when('/search/',
	        {
	            templateUrl:    'tpl/home.php',
	            controller: 	'jsonController'
	        <?php } ?>
	        }).otherwise({
	        	redirectTo: function() {
	        		<?php if($logged === true) { ?>
	        		return "/search";
	        		<?php } else { ?>
	        		return "/login";
	        		<?php } ?>
	        	}
	        });

	});

	app.controller("loginController", function($scope, $http) {
		$scope.systems = systems;
		$scope.loginload = true;
		$scope.loginfailed = false;
		$scope.login = function(data) {
			var data = data;
			$scope.loginload = false;
			if(data.user.length > 0) {
				if(data.pass.length > 0) {
					$http({
						method: 'POST',
						url: 'login.php',
						data: data,
						cache: false
					})
					.success(function(data) {
						if(data.login === true) {
			        		window.location = "index.php";
						} else {
							$scope.loginload = true;
							$scope.loginfailed = true;
						}
					})
					.error(function() {
						console.log("Login error");
					});
				}
			}
		};
	});

	app.controller("jsonController", ["$scope", "$http", "$routeParams", "$location", "$parse", function($scope, $http, $routeParams, $location, $parse) {
		$scope.master = [];
		$scope.showbar = false;$scope.showcon = false;$scope.button_clicked = false;$scope.display_advanced = false;$scope.cancelsearch = false;$scope.search_form = false;
		$scope.systems = systems;
		$scope.debugshow = false;

		$scope.searchTpl = "user";

		$scope.query = {limit: {start: 0, end: 20}};

		$scope.tabs = 1;

		$scope.updatePriv = function(userid, system, priv) {
			
		}

		$scope.getAllPriv = function(system) {
			$scope.privileges = "";

			var urlString = "http://elog.local/api/data.php?1t&s=api&t=privilege&q=get_all_privileges";

			$http({
				method: 'GET',
				url: urlString,
				cache: false
			})
			.success(function(data) {
				$scope.privileges = data.response;
				console.log(+new Date());
			})
			.error(function(data) {
				console.log("Privleges load error");
			});
		}

		$scope.loadPriv = function(userid, system) {
			$scope.priv = null;
			$scope.userprivilege = [];
			var urlString = "http://" + system + ".elogbooks.net/api/data.php?1t&s=api&t=privilege&q=get&filter[uid]=" + userid;

			$http({
				method: 'GET',
				url: urlString,
				cache: false
			})
			.success(function(data) {
				$scope.priv = data.response.rows;

				$scope.temppriv = {};

				angular.forEach($scope.priv, function(value, key) {
					$scope.temppriv[value.pid] = value.pid;
				});

				if($scope.privileges) {
					angular.forEach($scope.privileges, function(privalue, privkey) {
						if(privkey in $scope.temppriv) {
							$scope.userprivilege.push({privilege: privkey, value: true});
						} else {
							$scope.userprivilege.push({privilege: privkey, value: false});
						}
					});
				}
				/*
							if(privkey == value) {
								alert("true");
								$scope.userprivilege.push({privilege: privkey, value: true});
							} else {
								$scope.userprivilege.push({privilege: privkey, value: false});
							}
				*/
			})
			.error(function(data) {
				console.log("Priv. Error");
			})
		}

		$scope.logout = function(test) {
			$http({
				method: 'GET',
				url: 'logout.php',
				cache: false
			})
			.success(function(data) {
				if(data.logout === true) {
					window.location = 'index.php';
				}
			})
			.error(function(data) {
				console.log("error");
			});
		}

		$scope.unlock = function(userid, system) {
			var urlString = "&s=api&t=user&q=update&array[fla]=0&id="+userid;
			$http({
				method: 'GET',
				url: 'http://' + system + '.elogbooks.net/api/data.php?1t'+urlString,
				cache: false
			})
			.success(function(data) {
				if(data.key === "resource_updated") {
					alert("User unlocked");
				} else {
					alert("User not unlocked");
				}
			})
			.error(function(data) {
				consoloe.log("Update error");
			})
		}

		$scope.search = function(value, table) {
			console.log(+new Date());
			$scope.getAllPriv("elog");
			
			$scope.searchTpl = table;
			$scope.master = angular.copy(value);
			$scope.json = [];
			$scope.changes = [];

			if(!$scope.master.search) {
				$scope.json.push({empty: "empty search"});
				return false;
			} else {
				$scope.showbar = true;$scope.showcon = false;$scope.button_clicked = true;$scope.cancelsearch = true;$scope.search_form = true;
			}
			
			if($scope.master.systems) {
				angular.forEach($scope.master.systems, function(value, key) {
					if(value === true) {
						$scope.changes.push({"system" : key});
					}
				});
			}
			
			for (i in systems) {
				if($scope.changes.length === 0) {
					$scope.getData(systems[i].system, table, 'get')
				} else {
					angular.forEach($scope.changes, function(value, key) {
						if(value.system == systems[i].system) {
							$scope.getData(value.system, table, 'get');
						}
					});
				}
			}
		}

		$scope.getData = function(name, className, method) {
			var getT = className;
			var searchFilter = {};
			var stringUrl = "&s=api&t="+className+"&q="+method;

			for(c in tables) {
				angular.forEach(tables[c][getT], function(value, key) {
					var count = value.length;
					var anz = 1;
					angular.forEach(value, function(value, key) {
						if(anz < count) {
							var table = value.table + " LIKE";
							stringUrl = stringUrl + "&filter[" + table + "]=%"+$scope.master.search+"%";
							stringUrl = stringUrl + "&filter[]=OR";
						} else {
							var table = value.table + " LIKE";
							stringUrl = stringUrl + "&filter[" + table + "]=%"+$scope.master.search+"%";
						}
						anz++;
					});
				});
			}

			stringUrl = stringUrl + "&system=" + name + "&method=" + className + "&_ref";
			var name = name;

			$http({
				method: "GET",
				url: "http://" + name + ".elogbooks.net/api/data.php?&1t" + stringUrl, 
				cache: false
			})
			.success(function(data) {
				$scope.showbar = false;$scope.showcon = true;$scope.button_clicked = false;$scope.cancelsearch = false;$scope.search_form = false;
				var system = {system: data.params.system, result: []};
				system.result.push(data);
				system.result.push(data.params.system);
				$scope.json.push(system);
			})
			.error(function(data) {
				console.log("Error: " + data);
			});
		}
	}]);
