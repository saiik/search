	<?php
		session_start();
		if(empty($_SESSION["hashed"])) {
			die;
		}
	?>
	<div id="userinfo">
		<div id="uic">
			Logged in as: <?php echo $_SESSION["user"]; ?> on <?php echo $_SESSION["system"]; ?>
		</div>
	</div>
	<div id="wrapper">
		<div id="head"></div>
		<div id="searchFormHtml">
			<ul class="nav nav-tabs" role="tablist" id="myTab">
			  <li class="active"><a href role="tab" data-target="#u" data-toggle="tab">User</a></li>
			  <li><a href role="tab" data-target="#c" data-toggle="tab">Customers Sites</a></li>
			  <li><a href role="tab" data-target="#s" data-toggle="tab">Service Provider</a></li>
			  <li><a href ng-click="logout(1)">Logout</a></li>
			</ul>

			<div class="tab-content">
			  <div class="tab-pane active" id="u">
				<div class="panel panel-default">
					<div class="panel-body">
						<form novalidate>
							<input ng-disabled="search_form" required type="text" name="search" class="form-control" ng-model="user.search" id="searchinput" placeholder="Search for a user.." />
							<br />
							<center>
								<button type="button" class="btn btn-default" ng-click="button_clicked || search(user, 'user', 'get')" ng-disabled="button_clicked">Search</button>
							</center>
						</form>
					</div>
				</div>	
			  </div>
			  <div class="tab-pane" id="c">
				<div class="panel panel-default">
					<div class="panel-body">
						<form novalidate>
							<input ng-disabled="search_form" required type="text" name="search" class="form-control" ng-model="customer.search" id="searchinput" placeholder="Search for a customer site.." />
							<br />
							<center>
								<button type="button" class="btn btn-default" ng-click="button_clicked || search(customer, 'customer', 'get')" ng-disabled="button_clicked">Search</button>
							</center>
						</form>
					</div>
				</div>	
			  </div>
			  <div class="tab-pane" id="s">
				<div class="panel panel-default">
					<div class="panel-body">
						<form novalidate>
							<input ng-disabled="search_form" required type="text" name="search" class="form-control" ng-model="service.search" id="searchinput" placeholder="Search for a service provider.." />
							<br />
							<center>
								<button type="button" class="btn btn-default" ng-click="button_clicked || search(service, 'service', 'get')" ng-disabled="button_clicked">Search</button>
							</center>
						</form>
					</div>
				</div>	
			  </div>
			</div>

			<script>
			  $(function () {
			    $('#myTab a:first').tab('show')
			  })
			</script>
		</div>
		<div ng-show="test">
			<pre>
			debug {{master | json}}
			<hr />
			changes {{changes | json}}
			<hr />
			json {{json | json}}
			</pre>
		</div>
		<div ng-show="showbar" style="text-align: center; margin-top: 5%; opacity: 0.5;"><img src="style/img/ajax-loader.gif" alt="" /></div>
		<div ng-show="showcon" id="searchContent">
			<div ng-repeat="row in json">
				<div ng-repeat="result in row.result " style="margin-bottom: 30px;" ng-if="result.response.num_rows > 0">

				
					<div class="panel-group" id="accordion">
						<div class="panel panel-default"> <!-- accord -->
							<div class="panel-heading">
								 <h4 class="panel-title">
								 	<a data-toggle="collapse" data-parent="#accordion" data-target="#{{row.system}}" style="cursor: pointer;">
										<div ng-show="row.empty.length">No search value</div>
										<div>Search results for the system: <strong>{{row.system | uppercase}} <span ng-repeat="n_row in result"><span ng-if="n_row.num_rows > 0">({{n_row.num_rows}} results)</span><span ng-if="n_row.num_rows == 0">(0 results)</span></span></strong></div>
									</a>
								</h4>
							</div>
 							<div id="{{row.system}}" class="panel-collapse collapse">
 								 <div class="panel-body">
									<div ng-repeat="(key, value) in result">
										<div ng-include="'tpl/' + searchTpl + 's-template.html'"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!--
			<div ng-repeat="row in json">
				<div ng-show="row.empty.length">No search value</div>
				<div ng-hide="row.empty.length" ng-repeat="n_row in row.result"><span ng-show="n_row.response.rows.length">Search results for the system: <strong>{{row.system | uppercase}} <span>({{n_row.response.num_rows}} results)</span></span></strong></div><br />
				<div ng-repeat="results in row.result">
					<div ng-repeat="(key, value) in results">
						<div ng-include="'tpl/' + searchTpl + 's-template.html'"></div>
					</div>
				</div>
			</div>-->
		</div>
	</div>
