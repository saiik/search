<?php
error_reporting(0);
session_start();

$data = (array)json_decode(file_get_contents('php://input'), TRUE);

if(!empty($data["user"])) {
	if(!empty($data["pass"])) {
		if(empty($data["systemname"])) {
			echo json_encode(array("login" => false));
			die;
		}
		$user = $data["user"];
		$pass = $data["pass"];
		$system = $data["systemname"];

		$where["name"] = $user;
		$where[] = "AND";
		$where["passwd"] = $pass;

		$post = array("s" => "api", "t" => "user", "q" => "get", "filter[name]" => $user, "filter[]" => "AND", "filter[passwd]" => $pass);

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, "http://".$system.".elogbooks.net/api/data.php?1t");
		curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

		$result = json_decode(curl_exec($ch));

		curl_close($ch);

		if($result->response->num_rows == 1)  {
			$_SESSION["hashed"] = md5($result->response->row->uid);
			$_SESSION["user"] = $result->response->row->name;
			$_SESSION["system"] = $system;
			echo json_encode(array("login" => true));
		} else {
			echo json_encode(array("login" => false));
		}
	} else {
		echo json_encode(array("login" => false));
	}
} else {
	echo json_encode(array("login" => false));
}
