<!DOCTYPE html>
<html ng-app="searchApp">
	<head>
		<title>Global Search</title>
		<link rel=stylesheet href="style/css.css" type="text/css">
		<link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
		<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.20/angular.min.js"></script>
		<link rel="stylesheet" href="style/css/bootstrap.min.css">
		<link rel="stylesheet" href="style/css/bootstrap-theme.min.css">
		<script src="style/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="style/js/route.js"></script>
		<script type="text/javascript" src="style/js/search.php"></script>
	</head>
<body>
	<div>
		<div ng-view></div>
	</div>
</body>
</html>
