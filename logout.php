<?php
	session_start();
	unset($_SESSION["hashed"]);
	unset($_SESSION["user"]);
	unset($_SESSION["system"]);
	echo json_encode(array("logout" => true));
?>
